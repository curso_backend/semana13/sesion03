Django Rest Framework (DRF)

- Construir un API RESTful sobre Django de forma sencilla.
- Capa adicional sobre cualquier proyecto en Django.
- Django -> Proyectos Web
- DRF -> Añade una capa que permite enlazar URLs que contienen verbos HTTP y que retornan recursos
    en diversos formatos (JSON, XML, etc.).
- Serialización: Convierte lo recibido para luego retornarlo.
- Serializador: Intermediario entre la vista y el modelo o viceversa.
- Lo retornado, más que tener un formato (JSON o XML, por ejemplo) se debe definir una estructura.
- Form-data es el único tipo de formato que admite carga de archivos.
- Deserialización
- APIView -> Comportamiento parecido a la clase View de Django.
- Trabajando con cliente -> Clientes que consuman los datos del API.
- Seguridad: Tokens CSRF y cabeceras CORS.
- Si el cliente envía más datos de los que el API puede recibir, se genera un problemas en el CORS.
- Se puede resolver habilitando el poder recibir todo o en todo caso definiendole al cliente que
    verbos HTTP se van a considerar.